import sbt._

object Versions {
  final lazy val approval    = "0.13.4"
  final lazy val pprint      = "0.9.0"
  final lazy val refined     = "0.11.3"
  final lazy val scalaCompat = "2.13.0"
  final lazy val scalatest   = "3.2.19"
}

object Dependencies {
  final lazy val approval    = "com.colisweb"            % "approvals-java"          % Versions.approval
  final lazy val pprint      = "com.lihaoyi"            %% "pprint"                  % Versions.pprint
  final lazy val refined     = "eu.timepit"             %% "refined"                 % Versions.refined
  final lazy val scalaCompat = "org.scala-lang.modules" %% "scala-collection-compat" % Versions.scalaCompat
  final lazy val scalatest   = "org.scalatest"          %% "scalatest"               % Versions.scalatest
}
